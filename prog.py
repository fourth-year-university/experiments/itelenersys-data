import powerstand
import os

psm = powerstand.init()

with open("houses.txt", 'a') as f:
    print("Дома")
    print(psm.houses[0].value, psm.houses[1].value, file=f)    # float
    print(psm.houses[0].value, psm.houses[1].value)

with open("factories.txt", 'a') as f:
    print("Заводы")
    print(psm.factories[0].value, psm.factories[1].value, file=f)    # float
    print(psm.factories[0].value, psm.factories[1].value)

with open("hospitals.txt", 'a') as f:
    print("Больницы")
    print(psm.hospitals[0].value, psm.hospitals[1].value, file=f)    # float
    print(psm.hospitals[0].value, psm.hospitals[1].value)

with open("sun_gen.txt", 'a') as f:
    print("Солнечные панели")
    print(psm.sun_gens[0].value, psm.sun_gens[1].value, file=f)
    print(psm.sun_gens[0].value, psm.sun_gens[1].value)

with open("wind_gen.txt", 'a') as f:
    print("Ветряки")
    print(psm.wind_gens[0].value, psm.wind_gens[1].value, file=f)
    print(psm.wind_gens[0], psm.wind_gens[1].value)


with open("wind.txt", 'a') as f:
    print("Ветер")
    print(psm.wind.value, file=f)
    print(psm.wind.value)


with open("sun.txt", 'a') as f:
    print("Солнце")
    print(psm.sun.value, file=f)
    print(psm.sun.value)


print(os.getcwd())

psm.save_and_exit()
